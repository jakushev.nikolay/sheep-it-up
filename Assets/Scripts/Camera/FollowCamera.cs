using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Transform _target;
    private readonly float _height = 10f;
    private readonly float _damping = 2f;

    private Vector3 _startPosition;
    private bool _canFolow = false;
    public bool CanFolow { get => _canFolow; set => _canFolow = value; }

    private void Start()
    {
        _target = GameObject.FindWithTag("Player").transform;
        _startPosition = transform.position;
        CanFolow = true;
    }

    private void Update()
    {
        Follow();
    }

    private void Follow()
    {
        transform.position = _canFolow ? Vector3.Lerp(
                transform.position,
                new Vector3(_target.position.x + 10f, _target.position.y + _height, _target.position.z - 10f),
                Time.deltaTime * _damping
            ) : transform.position;
    }
}
