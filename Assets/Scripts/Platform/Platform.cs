using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private Transform[] _spikes;
    [SerializeField] private GameObject _coin;
    private bool _fallDown;
    private Coin coin;
    private Spike _spike;

    private void Start()
    {
        coin = GetComponent<Coin>();
        _spike = GetComponent<Spike>();
        ActivatePlatform();
    }

    private void ActivatePlatform()
    {
        int chance = Random.Range(0, 100);
        if (chance <= 70) { return; }
        int type = Random.Range(0, 8);
        switch (type)
        {
            case 0:
                _spike.ActivateSpike(_spikes);
                break;
            case 1:
                coin.ActivateCoin(_coin);
                break;
            case 2:
                _fallDown = true;
                break;
            case 3:                
                break;
            case 4:
                coin.ActivateCoin(_coin);
                break;
            case 5:
                break;
            case 6:                
                break;
            case 7:
                coin.ActivateCoin(_coin);
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (_fallDown)
            {
                _fallDown = false;
                Invoke("InvokeFalling", 2f);
            }
        }
    }

    private void InvokeFalling()
    {
        gameObject.AddComponent<Rigidbody>();
    }
}
