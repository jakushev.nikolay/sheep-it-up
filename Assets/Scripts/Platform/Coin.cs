using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    public void ActivateCoin(GameObject coin)
    {
        GameObject objectCoin = Instantiate(coin);
        objectCoin.transform.position = transform.position;
        objectCoin.transform.SetParent(transform);
        objectCoin.transform.DOLocalMoveY(1f, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameObject.SetActive(false);
            Sound.Instance.CoinSound();
            Gameplay.Instance.IncrementScore();
        }
    }
}
