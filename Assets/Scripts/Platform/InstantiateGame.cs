using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InstantiateGame : MonoBehaviour
{
    [SerializeField] private int _timeToSpawn = 100;
    [SerializeField] private GameObject[] _platform;
    [SerializeField] private GameObject _player;

    private readonly float _blockWidth = 0.5f;
    private readonly float _blockHeight = 0.2f;
    private readonly int _beginToTime = 0;
    private Vector3 _lastPosition;

    private readonly List<GameObject> _spawnedPlatforms = new List<GameObject>();
    
    private void Awake()
    {
        InstantiateLevel();
    }

    private void InstantiateLevel()
    {
        for (int i = _beginToTime; i < _timeToSpawn; i++)
        {
            GameObject newPlatform = (i == 0) ? Instantiate(_platform[0]) : (i == _timeToSpawn - 1 ? Instantiate(_platform[2]) : Instantiate(_platform[1]));
            newPlatform.transform.parent = transform;
            _spawnedPlatforms.Add(newPlatform);

            if (i == 0)
            {
                _lastPosition = newPlatform.transform.position;
                PlayerInstance(_lastPosition);
                continue;
            }

            int left = Random.Range(0, 2);
            newPlatform.transform.position = left == 0 ? new Vector3(_lastPosition.x - _blockWidth,
                                                                     _lastPosition.y + _blockHeight,
                                                                     _lastPosition.z) :
                                                         new Vector3(_lastPosition.x,
                                                                     _lastPosition.y + _blockHeight,
                                                                     _lastPosition.z + _blockWidth);

            _lastPosition = newPlatform.transform.position;
            if (i < 25)
            {
                float endPos = newPlatform.transform.position.y;
                newPlatform.transform.position = new Vector3(newPlatform.transform.position.x,
                                                             newPlatform.transform.position.y - _blockHeight * 3f,
                                                             newPlatform.transform.position.z);

                newPlatform.transform.DOLocalMoveY(endPos, .3f).SetDelay(i * .1f);
            }
        }
    }

    private void PlayerInstance(Vector3 lastPosition)
    {
        Vector3 temp = lastPosition;
        temp.y += .1f;
        Instantiate(_player, temp, Quaternion.identity);
    }
}
