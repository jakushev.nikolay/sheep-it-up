using UnityEngine;
using DG.Tweening;

public class Spike : MonoBehaviour
{
    public void ActivateSpike(Transform[] _spikes)
    {
        int index = Random.Range(0, _spikes.Length);
        _spikes[index].gameObject.SetActive(true);
        _spikes[index].DOLocalMoveY(.7f, 1.3f)
                      .SetLoops(-1, LoopType.Yoyo).SetDelay(Random.Range(3f, 5f));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.SetActive(false);
            Sound.Instance.EndSound();
            Gameplay.Instance.RestartGame();
        }
    }

}
