using UnityEngine;

public class Sound : MonoBehaviour
{    
    [SerializeField] private AudioSource[] _audioSource;
    public static Sound Instance;

    private void Awake()
    {
        if(Instance == null) { Instance = this; }
    }

    public void StartSound()
    {
        _audioSource[0].Play();
    }

    public void EndSound()
    {
        _audioSource[1].Play();
    }

    public void CoinSound()
    {
        _audioSource[2].Play();
    }

    public void JumpSound()
    {
        _audioSource[3].Play();
    }

}
