using UnityEngine;
using DG.Tweening;

public class Movement : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private readonly float movementForce = .5f;
    private readonly float jumpForce = .15f;
    private readonly float jumpTime = .15f;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        InitMovement();
    }

    private void InitMovement()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {           
            JumpLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {            
            JumpRight();
        }
    }

    private void JumpRight()
    {
        Sound.Instance.JumpSound();
        transform.DORotate(new Vector3(0f, -180f, 0f), 0f);
        _rigidbody.DOJump(new Vector3(transform.position.x, transform.position.y + jumpForce, transform.position.z + movementForce), .5f, 1, jumpTime); 
    }

    private void JumpLeft()
    {
        Sound.Instance.JumpSound();
        transform.DORotate(new Vector3(0f, 90f, 0f), 0f);
        _rigidbody.DOJump(new Vector3(transform.position.x - movementForce, transform.position.y + jumpForce, transform.position.z), .5f, 1, jumpTime);
    }
}
