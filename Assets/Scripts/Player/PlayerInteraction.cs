using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private bool _isDied = false;
    private FollowCamera _followCamera;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _followCamera = Camera.main.GetComponent<FollowCamera>();
    }

    private void Update()
    {
        CheckPlayerPositionOnPlatform();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("End Platform"))
        {
            Sound.Instance.StartSound();
            Gameplay.Instance.RestartGame();
        }
    }

    private void CheckPlayerPositionOnPlatform()
    {
        if (!_isDied)
        {
            if (_rigidbody.velocity.sqrMagnitude > 60)
            {
                _isDied = true;
                _followCamera.CanFolow = false;
                Sound.Instance.EndSound();
                Gameplay.Instance.RestartGame();
            }
        }
    }
}
