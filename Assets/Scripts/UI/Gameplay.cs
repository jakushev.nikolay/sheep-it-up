using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gameplay : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    private int _score;

    public static Gameplay Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void IncrementScore()
    {
        _score++;
        _scoreText.text = "x" + _score;
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void RestartGame()
    {
        Invoke("ReloadGame", 3f);
    }
}
